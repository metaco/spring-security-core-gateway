package club.example.securitycoreprice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecurityCorePriceApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecurityCorePriceApplication.class, args);
    }

}
