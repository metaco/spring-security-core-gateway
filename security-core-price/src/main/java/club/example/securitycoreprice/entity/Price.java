package club.example.securitycoreprice.entity;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;


@Setter
@Getter
public class Price {

    private Long id;

    private BigDecimal priceData;
}
