package club.example.securitycoreprice.controller;

import club.example.securitycoreprice.entity.Price;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@Slf4j
@RestController
@RequestMapping("/price")
public class PriceController {

    @GetMapping("/{id}")
     public Price getPrice(@PathVariable Long id) {
         log.info("productId is " + id);
         Price price = new Price();
         price.setId(id);
         price.setPriceData(new BigDecimal(100));
         return price;
     }
}
