package club.example.securitycoreoauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecurityCoreOauthApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecurityCoreOauthApplication.class, args);
    }

}
