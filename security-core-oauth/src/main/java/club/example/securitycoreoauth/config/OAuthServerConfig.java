package club.example.securitycoreoauth.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;

import javax.sql.DataSource;


@Configuration
@EnableAuthorizationServer
public class OAuthServerConfig extends AuthorizationServerConfigurerAdapter {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private DataSource dataSource;

    @Bean
    public TokenStore tokenStore() {
        return new JdbcTokenStore(dataSource);
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
//        // super.configure(clients);
//        clients.inMemory()
//                // 增加订单客户端授权
//                .withClient("orderApp")
//                // 基于客户端的请求的密码
//                .secret(passwordEncoder.encode("123456"))
//                // 权限ACL操作
//                .scopes("read", "write")
//                // 发送有效token 时间
//                .accessTokenValiditySeconds(3600)
//                // 代表资源服务器ID
//                .resourceIds("order-server")
//                // 授权方式
//                .authorizedGrantTypes("password")
//                .and()
//                // 增加订单服务授权
//                .withClient("orderService")
//                // 基于客户端的请求的密码
//                .secret(passwordEncoder.encode("123456"))
//                // 权限ACL操作
//                .scopes("read")
//                // 发送有效token 时间
//                .accessTokenValiditySeconds(3600)
//                // 代表资源服务器ID
//                .resourceIds("order-server")
//                // 授权方式
//                .authorizedGrantTypes("password");

        clients.jdbc(dataSource);
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints
                .tokenStore(tokenStore())
                .authenticationManager(authenticationManager);
    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        // 检查token 服务访问规则 , 检查token 的请求 ,必须经过认证的 (附带用户名&密码)
        security.checkTokenAccess("isAuthenticated()");
    }

    public static void main(String[] args) {
        System.out.println(new BCryptPasswordEncoder().encode("123456"));
    }
}
