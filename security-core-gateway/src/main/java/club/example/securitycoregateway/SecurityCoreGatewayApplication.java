package club.example.securitycoregateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableZuulProxy
public class SecurityCoreGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecurityCoreGatewayApplication.class, args);
    }

}
