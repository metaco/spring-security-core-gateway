package club.example.securitycoreorder.entity;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Order {

    private Long productId;
}
