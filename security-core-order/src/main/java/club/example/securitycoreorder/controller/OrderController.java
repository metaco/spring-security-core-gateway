package club.example.securitycoreorder.controller;


import club.example.securitycoreorder.entity.Order;
import club.example.securitycoreorder.entity.Price;
import club.example.securitycoreorder.security.UserDetail;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@Slf4j
@RestController
@RequestMapping("/order")
public class OrderController {

    private RestTemplate restTemplate = new RestTemplate();

    @PostMapping
    public Order create(@RequestBody  Order order, @AuthenticationPrincipal UserDetail userDetail) {
//        Price price = restTemplate
//                .getForObject("http://localhost:9080/price/" + order.getProductId(), Price.class);
//
//        log.info("price is " + price.getPriceData());
        log.info("authentication username = {}", userDetail.getUsername());

        return order;
    }

    @GetMapping("/{id}")
    public Order get(@PathVariable Long id) {
        log.info("get id = {}", id);
        Order order =  new Order();
        order.setProductId(id);

        return order;
    }
}
