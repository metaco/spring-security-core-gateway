package club.example.securitycoreorder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecurityCoreOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecurityCoreOrderApplication.class, args);
    }

}
